from api.models import FeedItem
from rest_framework import viewsets
from api.serializers import FeedSerializer


class FeedItemViewSet(viewsets.ModelViewSet):
    queryset = FeedItem.objects.all()
    serializer_class = FeedSerializer
