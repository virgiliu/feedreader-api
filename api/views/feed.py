from api.models import Feed
from rest_framework import viewsets
from api.serializers import FeedSerializer


class FeedViewSet(viewsets.ModelViewSet):
    queryset = Feed.objects.all()
    serializer_class = FeedSerializer
