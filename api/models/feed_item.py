from django.db import models

class FeedItem(models.Model):
    class Meta:
        app_label = 'api'

    parent = models.ForeignKey('Feed')

    # Creation in FeedReader DB
    created_at = models.DateTimeField(auto_now_add=True)

    # Update in FeedReader DB
    updated_at = models.DateTimeField(auto_now=True)

     # The feed item ID given by the feed's source
    external_id = models.CharField(max_length=255, blank=True, null=True)
    title = models.CharField(max_length=255)
    body = models.TextField(blank=True, null=True)
    link = models.CharField(max_length=2043, blank=True, null=True)
    author = models.CharField(max_length=255, blank=True, null=True)
    published_at = models.DateTimeField(blank=True, null=True)

    # Used for dumb servers which don't support ETag and/or Last-Modified
    item_hash = models.CharField(max_length=255, blank=True)

    def __str__(self):
        # Body might not be defined
        body = getattr(self, 'body', None)

        if body:
            body = body[:30]

        return '%s: %s' % (self.title, body)
