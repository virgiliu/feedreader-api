from django.db import models

class Feed(models.Model):
    class Meta:
        app_label = 'api'

    url = models.URLField(unique=True)
    added_at = models.DateTimeField(auto_now_add=True)
    refreshed_at = models.DateTimeField(blank=True, null=True)

    etag = models.CharField(max_length=255)

    # This field is used to store the feed's own "modified at" value
    # It's going to be used when requesting new items
    modified = models.CharField(max_length=255)

    def __str__(self):
        return self.url
