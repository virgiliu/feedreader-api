from .models import Feed, FeedItem
from rest_framework import serializers

class FeedSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Feed
        fields = ('id', 'url', 'refreshed_at')

class FeedItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FeedItem
        fields = ('id', 'parent', 'external_id', 'title', 'body', 'link',
                  'author', 'published_at', 'item_hash')
